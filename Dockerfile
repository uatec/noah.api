FROM microsoft/aspnet:latest

RUN apt-get install -y git

ADD . /app/

WORKDIR /app/src/noah.api

RUN kpm restore
RUN kpm build
 
EXPOSE 5004
ENTRYPOINT ["k", "kestrel"]
