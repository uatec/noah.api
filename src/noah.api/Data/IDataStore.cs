using System;
using System.Collections.Generic;
using noah.api.Models.Domain;

namespace noah.api.Data
{
    public interface IDataStore
    {
        void SetRegistry(string repoUrl, string registryAddress);
        Repository GetRepository(string repoUrl);
        IEnumerable<Repository> GetRepositories();
        IEnumerable<Build> GetBuildsByRepository(string repoUrl);
        IDictionary<string, List<Build>> GetAllBuilds();
        Build GetBuild(string buildId);
        void AddBuild(Build build);
        void SetBuildState(string buildId, BuildState buildState);
    }
}