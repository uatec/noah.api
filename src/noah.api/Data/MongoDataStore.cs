using System;
using System.Collections.Generic;
using noah.api.Models.Domain;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Linq;

namespace noah.api.Data
{
	public class MongoDataStore : IDataStore
    {
    	private readonly MongoClient _client;
    	private readonly string _databaseName;

    	public MongoDataStore(string connectionString, string databaseName)
    	{
    		_databaseName = databaseName;
			_client = new MongoClient(connectionString);
    	}

        public void SetRegistry(string repoUrl, string registryAddress)
        {
        	_client
        		.GetDatabase(_databaseName)
        		.GetCollection<Repository>("repositories")
        		.UpdateOneAsync(
					Builders<Repository>.Filter.Eq(e => e.Url, repoUrl),
					Builders<Repository>.Update.Set(e => e.RegistryAddress, registryAddress));
        }

        public Repository GetRepository(string repoUrl)
        {
        	return _client
        		.GetDatabase(_databaseName)
        		.GetCollection<Repository>("repositories")
        		.Find(e => e.Url.Equals(repoUrl))
        		.ToListAsync()
        		.Result
        		.Single();
        }

        public Build GetBuild(string buildId)
        {
        	return _client
        		.GetDatabase(_databaseName)
        		.GetCollection<Build>("builds")
        		.Find(e => e.Id == buildId)
        		.ToListAsync()
        		.Result
        		.Single();
        }


        public void AddBuild(Build build)
        {
			_client
        		.GetDatabase(_databaseName)
        		.GetCollection<Build>("builds")
        		.InsertOneAsync(build);

        	_client
        		.GetDatabase(_databaseName)
        		.GetCollection<Repository>("repositories")
        		.UpdateOneAsync(
					Builders<Repository>.Filter.Eq(e => e.Id, build.RepoUrl),
					Builders<Repository>.Update.Set(e => e.Name, build.RepoName),
                    new UpdateOptions 
                    {
                        IsUpsert = true
                    }
        			);
        }

        public void SetBuildState(string buildId, BuildState buildState)
        {
			_client
        		.GetDatabase(_databaseName)
        		.GetCollection<Build>("builds")
        		.UpdateOneAsync(
					Builders<Build>.Filter.Eq(e => e.Id, buildId),
					Builders<Build>.Update.Set(e => e.BuildState, buildState));
        }

        public IEnumerable<Repository> GetRepositories()
        {
        	return _client
        		.GetDatabase(_databaseName)
        		.GetCollection<Repository>("repositories")
        		.Find(a => true)
        		.ToListAsync()
        		.Result;
        }

        public IDictionary<string, List<Build>> GetAllBuilds()
        {
            return _client
                .GetDatabase(_databaseName)
                .GetCollection<Build>("builds")
                .Find(a => true)
                .ToListAsync()
                .Result
                .GroupBy(a => a.RepoUrl)
                .ToDictionary(a => a.Key, a => a.ToList());
        }

        public IEnumerable<Build> GetBuildsByRepository(string repoUrl)
        {
            System.Console.WriteLine("Getting builds for: " + repoUrl);
            return _client
                .GetDatabase(_databaseName)
                .GetCollection<Build>("builds")
                .Find(b => b.RepoUrl.Equals(repoUrl))
                .ToListAsync()
                .Result;
        }
    }
}