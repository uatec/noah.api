using System.Collections.Concurrent;
using System.Collections.Generic;
using System;
using System.Linq;

namespace noah.api.Data
{
	public class MemoryLogStore : ILogStore
	{
		private ConcurrentDictionary<string, ConcurrentBag<Tuple<DateTime, string>>> _data = 
			new ConcurrentDictionary<string, ConcurrentBag<Tuple<DateTime, string>>>();

		public IEnumerable<string> Get(string buildId, int count = 1000, int offset = 0)
		{
			return _data[buildId]
				.OrderBy(a => a.Item1)
				.Skip(offset)
				.Take(count)
				.Select(x => x.Item2);
		}

		public void Append(string buildId, DateTime eventDateTime, string data)
		{
			_data.GetOrAdd(buildId, new ConcurrentBag<Tuple<DateTime, string>>())
				.Add(Tuple.Create(eventDateTime, data));
		}
	}
}