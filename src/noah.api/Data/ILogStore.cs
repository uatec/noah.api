using System.Collections.Generic;
using System;

namespace noah.api.Data
{
	public interface ILogStore
	{
		IEnumerable<string> Get(string buildId, int count = 1000, int offset = 0);
		void Append(string buildId, DateTime eventDateTime, string data);
	}
}