using System;

namespace noah.api.Events
{
	public class GitPushEvent
	{
		public string BuildId { get; set; }
		public GitRepository Repository { get; set; }
	}

	public class GitRepository
	{
		public string Full_Name { get; set; }
		public string Url { get; set; }
	}
}