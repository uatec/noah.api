using System;

namespace noah.api.Events
{
	public class BuildStartedEvent
	{
		public BuildStartedEvent()
		{
			this.DateTime = DateTime.Now;
		}

		public DateTime DateTime { get; set; }
		public string BuildId { get; set; } 
		public string RepositoryName { get; set; }
		public string RepositoryUrl { get; set; }
	}
}