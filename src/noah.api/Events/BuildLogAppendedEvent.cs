using System;

namespace noah.api.Events
{
	public class BuildLogAppendedEvent
	{
		public BuildLogAppendedEvent()
		{
			this.DateTime = DateTime.Now;
		}

		public DateTime DateTime { get; set; }
		public string BuildId { get; set; }
		public string Data { get; set; }
	}
}