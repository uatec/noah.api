using System;

namespace noah.api.Events
{
	public class BuildCompletedEvent
	{
		public BuildCompletedEvent()
		{
			this.DateTime = DateTime.Now;
		}

		public DateTime DateTime { get; set; }
		public string BuildId { get; set; }
	}
}