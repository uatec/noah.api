using System;

namespace noah.api.Events
{
	public class BuildFailedEvent
	{
		public BuildFailedEvent()
		{
			this.DateTime = DateTime.Now;
		}

		public DateTime DateTime { get; set; }
		public string BuildId { get; set; } 

		public string Message { get; set; }
		public string Source { get; set; }
		public string StackTrace { get; set; }
	}
}