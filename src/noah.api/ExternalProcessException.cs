using System;

namespace noah.api
{
	public class ExternalProcessException : Exception
	{
		public String ProcessName { get; private set; }
		public String Arguments { get; private set; }
		public int ExitCode { get; private set; }

		public ExternalProcessException(string processName, 
			string arguments, 
			int exitCode)
			: base("Process exited with non-zero exit code.")
		{
			this.ProcessName = processName;
			this.Arguments = arguments;
			this.ExitCode = exitCode;
		}
	}
}