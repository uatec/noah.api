using Microsoft.Framework.ConfigurationModel;
//using System.Linq;
using System.Diagnostics;
using System.IO;
//using Newtonsoft.Json.Linq;
//using Newtonsoft.Json;
using System;

using noah.api;
using noah.api.Events;

namespace noah.api.Subscribers
{
	public class Builder : ISubscriber<GitPushEvent>
	{
        private readonly IConfiguration _configuration;
        private readonly IBus _bus;

        private static Lazy<string> _dockerBinaryPath = 
            new Lazy<string>(() => {
                string os = null;
                if ( Directory.Exists("/Applications")) 
                {
                    os = "mac";
                }
                else if ( Directory.Exists("%SYSTEMROOT%") )
                {
                    os = "windows";
                    throw new NotImplementedException("OS Detected as Windows, but no docker binary is currently available for that platform.");
                }
                else
                {
                    os = "linux";
                }
                return "../../docker_" + os;
            });

        private string DockerBinaryPath
        {
            get {
                return _dockerBinaryPath.Value;
            }
        }

        public Builder(IConfiguration configuration, IBus bus)
        {
            _configuration = configuration;
            _bus = bus;
        }

        private void executeProcess(string eventId, string fileName, string arguments)
        {
            Action<object, DataReceivedEventArgs> handleOutputEvent = (sender, args)  => 
                {
                    if ( args.Data != null )
                    {
                        _bus.Publish(new BuildLogAppendedEvent 
                            {
                                Data = args.Data,
                                BuildId = eventId
                            });
                    }
                };

            Process p = new Process();
            p.StartInfo.FileName = fileName;
            p.StartInfo.Arguments = arguments;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.OutputDataReceived += new DataReceivedEventHandler(handleOutputEvent);
            p.ErrorDataReceived += new DataReceivedEventHandler(handleOutputEvent);
            p.Start();
            p.BeginOutputReadLine();
            p.BeginErrorReadLine();
            p.WaitForExit();
            if ( p.ExitCode != 0 )
            {
                throw new ExternalProcessException(
                    fileName,
                    arguments,
                    p.ExitCode
                );
            }
        }

        private void cloneRepository(string url, string target, string eventId)
        {
    	    executeProcess(eventId, "git", string.Format("clone \"{0}\" \"{1}\"", url, target));	
        }

        private void buildImage(string tag, string target, string eventId)
        {
            executeProcess(eventId, DockerBinaryPath, string.Format("build -t {0} {1}", tag.ToLower(), target));
        }

		public void Handle(GitPushEvent @event)
        {
            try 
            {
                _bus.Publish(new BuildStartedEvent
                    {
                        BuildId = @event.BuildId,
                        RepositoryName = @event.Repository.Full_Name,
                        RepositoryUrl = @event.Repository.Url
                    });

                innerHandler(@event);

                _bus.Publish(new BuildCompletedEvent
                    {
                        BuildId = @event.BuildId
                    });
            }
            catch (ExternalProcessException ex)
            {
                _bus.Publish(new BuildFailedEvent
                    {
                        BuildId = @event.BuildId,
                        Message = string.Format("Process '{0}' with arguments '{1}' exited with non-zero exit code: {2}",
                            ex.ProcessName,
                            ex.Arguments,
                            ex.ExitCode),
                        Source = ex.ProcessName,
                        StackTrace = ex.StackTrace
                    });
            }
            catch (Exception ex)
            {
                _bus.Publish(new BuildFailedEvent
                    {
                        BuildId = @event.BuildId,
                        Message = ex.Message,
                        StackTrace = ex.StackTrace
                    });
            }
        }

		private void innerHandler(GitPushEvent @event)
        {
            string repoUrl = @event.Repository.Url;
            string name = @event.Repository.Full_Name;

            // create working directory
            string workingDirectory = 
                _configuration.Get("build:path") ??
                Path.GetTempPath();

            string repoDir = Guid.NewGuid().ToString();
            string fullRepoDir = Path.Combine(workingDirectory, repoDir);
            Directory.CreateDirectory(fullRepoDir);

            // clone repository 
            cloneRepository(repoUrl, fullRepoDir, @event.BuildId);

            // build image
            buildImage(name.ToLower(), fullRepoDir, @event.BuildId);

            // Push to registry

            // tidy up afterwards
            Directory.Delete(fullRepoDir, true);
		}
	}
}