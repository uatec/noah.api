using noah.api.Events;
using noah.api.Data;

namespace noah.api.Subscribers
{
	public class LogRecorder : ISubscriber<BuildLogAppendedEvent>
	{
		private readonly ILogStore _logStore;

		public LogRecorder(ILogStore logStore)
		{
			_logStore = logStore;
		}

		public void Handle(BuildLogAppendedEvent @event)
		{
			_logStore.Append(
				@event.BuildId,
				@event.DateTime,
				@event.Data);
		}
	}
}