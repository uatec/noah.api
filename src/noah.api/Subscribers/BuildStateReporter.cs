using noah.api.Events;
using noah.api.Models.Domain;
using noah.api.Data;

namespace noah.api.Subscribers
{
	public class BuildStateReporter : 
		ISubscriber<GitPushEvent>,
		ISubscriber<BuildStartedEvent>,
		ISubscriber<BuildCompletedEvent>,
		ISubscriber<BuildFailedEvent>
	{
		private readonly IDataStore _dataStore;
		
		public BuildStateReporter(IDataStore dataStore)
		{
			_dataStore = dataStore;
		}

		public void Handle(GitPushEvent @event)
		{	
			_dataStore.AddBuild(new Build {
					Id = @event.BuildId,
					RepoUrl = @event.Repository.Url,
					RepoName = @event.Repository.Full_Name,
					BuildState = BuildState.Pending
				});
		}

		public void Handle(BuildStartedEvent @event)
		{
			_dataStore.SetBuildState(@event.BuildId, BuildState.Running);
		}

		public void Handle(BuildCompletedEvent @event)
		{
			_dataStore.SetBuildState(@event.BuildId, BuildState.Completed);
		}

		public void Handle(BuildFailedEvent @event)
		{
			_dataStore.SetBuildState(@event.BuildId, BuildState.Failed);
		}
	}
}