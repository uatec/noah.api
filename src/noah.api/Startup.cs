using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Routing;
using Microsoft.Framework.DependencyInjection;
using Microsoft.AspNet.Diagnostics;
using Microsoft.Framework.ConfigurationModel;
using Microsoft.AspNet.Hosting;
using Newtonsoft.Json;
using System;

using noah.api.Subscribers;
using noah.api.Events;
using noah.api.Data;

namespace noah.api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            // Setup configuration sources.
            Configuration = new Configuration()
                .AddJsonFile("config.json")
                .AddEnvironmentVariables();

            DataStore = new MongoDataStore(
                Configuration.Get("connectionstring"),
                Configuration.Get("databaseName"));

            LogStore = new MemoryLogStore();

            Bus = new MemoryBus();
            Bus.Subscribe(new Builder(this.Configuration, this.Bus));
            var buildStateReporter = new BuildStateReporter(this.DataStore);
            Bus.Subscribe<GitPushEvent>(buildStateReporter);
            Bus.Subscribe<BuildStartedEvent>(buildStateReporter);
            Bus.Subscribe<BuildCompletedEvent>(buildStateReporter);
            Bus.Subscribe<BuildFailedEvent>(buildStateReporter);
            Bus.Subscribe<BuildLogAppendedEvent>(new LogRecorder(this.LogStore));
            // Bus.Subscribe<object>(a => 
            //     Console.WriteLine(JsonConvert.SerializeObject(a)));
        }
 
        public IConfiguration Configuration { get; set; }
        public IBus Bus { get; set; }
        public IDataStore DataStore { get; set; }
        public ILogStore LogStore { get; set; }
        public void Configure(IApplicationBuilder app)
        {
            app.UseStaticFiles(); 
            
            app.UseErrorPage(ErrorPageOptions.ShowAll);

            app.UseServices(services =>
            {
                services.AddMvc();

                services.AddInstance(this.Configuration);
                services.AddInstance(this.Bus);
                services.AddInstance(this.DataStore);
                services.AddInstance(this.LogStore);
            });

            // Add MVC to the request pipeline
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action}/{id?}",
                    defaults: new { controller = "Event", action = "Index" });

                routes.MapRoute(
                    name: "api",
                    template: "{controller}/{id?}");
            });
        }
    }
}
