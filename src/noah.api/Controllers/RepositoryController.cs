using Microsoft.AspNet.Mvc;
using noah.api.Data;
using System.Net;

namespace noah.api.Controllers
{
	public class RepositoryController : Controller
	{
		private readonly IDataStore _dataStore;

		public RepositoryController(IDataStore dataStore)
		{
			_dataStore = dataStore;
		}

		public IActionResult SetRegistry(string id, string registryAddress)
		{
			string repoUrl = id;

			_dataStore.SetRegistry(repoUrl, registryAddress);
			return Content("ok");
		}

		public IActionResult GetAllBuilds()
		{
			return Json(_dataStore.GetAllBuilds());
		}

		public IActionResult GetBuilds(string id)
		{
			id = WebUtility.UrlDecode(id);
			return Json(_dataStore.GetBuildsByRepository(id));
		}

		public IActionResult Get(string id)
		{
			return Json(_dataStore.GetRepository(id));
		}

		public IActionResult Index()
		{
			return Json(_dataStore.GetRepositories());
		}
	}
}