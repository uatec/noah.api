﻿using System;
using Microsoft.AspNet.Mvc;

using noah.api;
using noah.api.Events;

namespace noah.api.Controllers
{
    public class EventController : Controller
    {
        private readonly IBus _bus;

        public EventController(IBus bus)
        {
            _bus = bus;
        }

        [HttpPost]
        public ActionResult Index([FromBody] GitPushEvent gitPushEvent)
        {
            gitPushEvent.BuildId = Guid.NewGuid().ToString();

            _bus.Publish(gitPushEvent);

            return Json(new {
                    BuildId = gitPushEvent.BuildId
                });
        }
    }
}
