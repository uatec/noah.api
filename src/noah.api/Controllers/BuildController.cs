using System;
using Microsoft.AspNet.Mvc;

using noah.api.Data;

namespace noah.api.Controllers
{
	public class BuildController : Controller
	{
		private readonly IDataStore _dataStore;

		public BuildController(IDataStore dataStore)
		{
			_dataStore = dataStore;
		}

		public IActionResult Get(string id)
		{
			return Json(_dataStore.GetBuild(id));
		}
	}
}