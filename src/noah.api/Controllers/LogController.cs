using Microsoft.AspNet.Mvc;
using noah.api.Data;
using System.Linq;
using System;

namespace noah.api.Controllers
{
	public class LogController : Controller
	{
		private readonly ILogStore _logStore;

		public LogController(ILogStore logStore)
		{
			_logStore = logStore;
		}

		public IActionResult GetLog(string id, int count = 10000, int offset = 0)
		{
			return Content(_logStore
				.Get(id, count, offset)
				.Aggregate((a, b) => a + Environment.NewLine + b));
		}
	}
}