namespace noah.api.Models.Domain
{
	public enum BuildState
	{
		Pending,
		Running,
		Completed,
		Failed
	}
}