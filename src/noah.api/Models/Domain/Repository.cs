
namespace noah.api.Models.Domain
{
	public class Repository
	{
		// Id is merely an alias for Url since the URL is the natural key,
		// but Id is a more useful term
		public string Id 
		{ 
			get { return this.Url; }
			set { this.Url = value; }
		}
		public string Url { get; set; }
		public string Name { get; set; }
		public string RegistryAddress { get; set; }
	}
}