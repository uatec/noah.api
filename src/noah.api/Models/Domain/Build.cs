using System;

namespace noah.api.Models.Domain
{
	public class Build
	{
		public string Id { get; set; }

		public string RepoUrl { get; set; }
		public string RepoName { get; set; }
		public BuildState BuildState { get; set; }
	}
}